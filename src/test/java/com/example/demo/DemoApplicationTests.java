package com.example.demo;

import static io.restassured.RestAssured.get;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@LocalServerPort
	private int port;

	@Test
	public void contextLoads() {}

	@Test
	public void serializableOnLocal() {
		get("http://localhost:" + port + "/withHandler/throwSerializable");
	}

	@Test
	public void serializableOnGlobal() {
		get("http://localhost:" + port + "/withoutHandler/throwSerializable");
	}

	@Test
	public void suppressedOnLocal() {
		get("http://localhost:" + port + "/withHandler/throwSuppressed");
	}

	@Test
	public void suppressedOnGlobal() {
		get("http://localhost:" + port + "/withoutHandler/throwSuppressed");
	}
}
