package com.example.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController
@RequestMapping("withHandler")
public class WithHandlerController extends AbstractController {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleLocal(Exception exception, HttpServletRequest request) {
	    System.err.format("%-34s %-24s %-12s\n", request.getRequestURI(), getClass().getSimpleName(), "handleLocal");
		return ResponseEntity.badRequest().body(exception.getMessage());
	}
}
