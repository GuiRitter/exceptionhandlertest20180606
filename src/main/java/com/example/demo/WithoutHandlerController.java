package com.example.demo;

import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController
@RequestMapping("withoutHandler")
public class WithoutHandlerController extends AbstractController {}
