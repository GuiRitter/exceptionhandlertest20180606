package com.example.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@ControllerAdvice
@EnableWebMvc
public class DefaultExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleGlobal(Exception exception, HttpServletRequest request) {
	    System.err.format("%-34s %-24s %-12s\n", request.getRequestURI(), getClass().getSimpleName(), "handleGlobal");
		return ResponseEntity.badRequest().body(exception.getMessage());
	}
}
