package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.NestedServletException;

public class AbstractController  {

	@GetMapping("throwSerializable")
	public ResponseEntity<Object> throwSerializable() throws NestedServletException {

		throw new NestedServletException("");
	}

//	@GetMapping("throwSuppressed")
//	public ResponseEntity<Object> throwSuppressed() throws MissingServletRequestParameterException {
//
//		throw new MissingServletRequestParameterException("", "");
//	}

	@GetMapping("throwSuppressed")
	public ResponseEntity<Object> throwSuppressed(@RequestParam(value = "param", required = true) Long param)  {

		return ResponseEntity.ok().build();
	}
}
